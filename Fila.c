#include <stdio.h>
#include <stdlib.h>

int tam = 0;

typedef struct fila Fila;
struct fila{
    int valor;
    Fila* prox;
};

Fila* aloca(){
    return (Fila*)malloc(sizeof(Fila));
}

int vazia(Fila* cabeca){
    if(cabeca->prox==NULL){ return 1;}
    else {return 0;}
}

void insere(Fila* cabeca, int val){
    Fila* novo = aloca();
    novo->valor = val;
    novo->prox = NULL;
    while(cabeca->prox != NULL){
        cabeca = cabeca->prox;
    }
    cabeca->prox = novo;
    tam++;
}

void exibe(Fila* cabeca){
    while(cabeca!=NULL){
        printf("%d ",cabeca->valor);
        cabeca = cabeca->prox;
    }
}

void tirar(Fila* cabeca){
    Fila *aux = cabeca->prox;
    cabeca->prox = (cabeca->prox)->prox;
    free(aux);
    tam--;
}

void salvar(Fila* cabeca){
    //salvando a fila
    FILE *arq;
    arq = fopen("fila.txt","wb");
    int vals[tam],i=0,total,total_tam;
    if(arq == NULL){
        printf("Erro ao salvar a fila.\n");
    }
    Fila *aux = cabeca->prox;
    while(aux != NULL){
        vals[i]=aux->valor;
        aux = aux->prox;
        i++;
    }
    total = fwrite(vals,sizeof(int),tam,arq);
    fclose(arq);

    //salvando o tamanho em outro arquivo
    FILE *arq_tam;
    arq_tam = fopen("tam_fila.txt","wb");
    fprintf(arq_tam,"%d",tam);
    if(total==tam){
        printf("Dados gravados com sucesso!\n");
    }
    else{printf("Erro!\n");}
}

void ler(Fila *cabeca){
    FILE *tam_arq = fopen("tam_fila.txt","rb");
    if(tam_arq == NULL){
        fclose(tam_arq);
        return;
    }
    int tam_aux;
    fscanf(tam_arq,"%d",&tam_aux);
    fclose(tam_arq);


    FILE *arq = fopen("fila.txt","rb");
    if(arq == NULL){
        printf("Nao foi possivel carregar a fila salva.\n\n");
        fclose(arq);
        return;
    }
    int vals[tam_aux],lidos,i;
    Fila *aux = cabeca->prox;
    lidos = fread(vals,sizeof(int),tam_aux,arq);
    if(lidos==tam_aux){
        for(i=0;i<tam_aux;i++){
            insere(cabeca,vals[i]);
        }
    }
    printf("Fila salva foi carregada com exito.\n\n");
    fclose(arq);
}

void menu(Fila* cabeca){
    int op = 0;
    int val;
    do{
        printf("----------- FILA ------------\n");
        printf("1 - Inserir na fila\n");
        printf("2 - Mostrar fila\n");
        printf("3 - Tirar primeiro\n");
        printf("4 - Esvaziar fila\n");
        printf("5 - Salvar fila\n");
        printf("0 - Sair\n\n");
        printf("TAMANHO DA FILA: %d\n",tam);
        printf("Digite a opcao: ");
        scanf("%d",&op);
        switch(op){
            case 1:
                system("cls");
                printf("Valor a inserir: ");
                scanf("%d",&val);
                insere(cabeca,val);
                system("cls");
                printf("Valor inserido\n");
                break;
            case 2:
                if(vazia(cabeca)){
                    system("cls");
                    printf("Fila vazia!\n");
                }
                else{
                    system("cls");
                    exibe(cabeca->prox);
                    printf("\n");
                    system("pause");
                    system("cls");
                }
                break;
            case 3:
                if(vazia(cabeca)){
                    system("cls");
                    printf("Fila vazia!\n");
                }
                else{
                    tirar(cabeca);
                    system("cls");
                    printf("Item removido da fila\n");
                }
                break;
            case 4:
                if(vazia(cabeca)){
                    system("cls");
                    printf("Fila ja esta vazia!\n");
                }
                else{
                    while(!vazia(cabeca)){
                        tirar(cabeca);
                    }
                    system("cls");
                    printf("Fila esvaziada!\n");
                }
                break;
            case 5:
                if(vazia(cabeca)){
                    system("cls");
                    printf("Nada para salvar. Fila vazia!\n");
                }
                else{
                    salvar(cabeca);
                }
                break;
            default:
                system("cls");
                printf("Opcao invalida\n\n");
                break;
        }
    }while(op != 0);
    system("cls");
    printf("Ate mais!\n");
}



int main(){
    Fila *cabeca=aloca();
    cabeca->prox=NULL;
    ler(cabeca);
    menu(cabeca);
    return 0;
}
