#include <stdlib.h>
#include <stdio.h>
#include <time.h>

typedef struct cell celula;
struct cell{
    int valor;
    celula *prox;
};

void mostra_lista(celula *le){
    if( le != NULL ){
        printf( "%d, ", le->valor );
        mostra_lista( le->prox );
    }
}

celula* procurar(celula *le, int val){
    if( le!= NULL && le->valor == val){
        return le;
    }
    else if (le!=NULL && le->valor !=val){
        procurar(le->prox,val);
    }
    else{return 0;}
}

void adiciona(celula *cabeca, int val){
    celula *novo, *aux;

    aux = cabeca;
    novo = (celula * )malloc(sizeof(celula));

    novo->valor = val;
    novo->prox = NULL;

    while(aux->prox != NULL){
        aux = aux->prox;
    }
    aux->prox = novo;
}


void excluir(celula *cabeca, int val){
    celula *item, *anterior;
    while( (cabeca->prox)->valor != val ){
        cabeca = cabeca->prox;
    }
    anterior = cabeca;
    item = cabeca->prox;
    anterior->prox = item->prox;
    free(item);
}

void menu(celula lista){
    int op = 0, val;
    do{
        printf("------LISTA ENCADEADA-------\n");
        printf("1 - Adicionar item\n");
        printf("2 - Mostrar todos os itens\n");
        printf("3 - Procurar um item\n");
        printf("4 - Excluir item\n");
        printf("0 - Sair\n");
        printf("Digite a opcao: ");
        scanf("%d",&op);
        switch(op){
            case 1:
                system("cls");
                printf("Digite o item que quer adicionar: ");
                scanf("%d",&val);
                if(procurar(lista.prox, val)){
                    printf("%d ja esta na lista\n\n",val);
                    system("pause");
                    system("cls");
                }
                else{
                    adiciona(&lista, val);
                    system("cls");
                    printf("Item adicionado!\n");
                }
                break;
            case 2:
                system("cls");
                if(lista.prox == NULL){printf("Lista vazia");}
                else{mostra_lista(lista.prox);}
                printf("\n");
                system("pause");
                system("cls");
                break;
            case 3:
                system("cls");
                printf("Valor a ser buscado: ");
                scanf("%d",&val);
                if(procurar(lista.prox,val)){printf("\nEsta na lista!\n");}
                else{printf("\nNao esta na lista\n");}
                break;
            case 4:
                system("cls");
                printf("Digite o item: ");
                scanf("%d",&val);
                if(procurar(lista.prox, val)){
                    excluir(&lista, val);
                    printf("Item excluido\n");
                }
                else{printf("Este item nao esta na lista!\n");}
                break;
            default:
                system("cls");
                printf("Opcao invalida\n\n");
                break;
        }
    }while(op != 0);
    system("cls");
    printf("Ate mais!\n");
}

int main(){
    celula lista;
    lista.prox = NULL;
    menu(lista);
    return 0;
}
