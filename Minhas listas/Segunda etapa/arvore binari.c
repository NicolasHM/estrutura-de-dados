#include <stdio.h>
#include <stdlib.h>

typedef struct no No;
struct no{
    struct no *dir;
    struct no *esq;
    int valor;
};

void criaArvVaz(No **raiz){
    *raiz= (No*)malloc(sizeof(No));
    if(*raiz!=NULL){
        *raiz=NULL;
    printf("Arvore criada com sucesso!\n");
    }
}

void inserir(No **raiz, int num){
    if(*raiz==NULL){ /* Caso a raiz seja nula, o elemento eh inserido nela */
        No *aux = (No *)malloc(sizeof(No));
        aux->valor=num;
        aux->dir = aux->esq = NULL;
        *raiz=aux;
        printf("\nO elemento %d foi inserido com sucesso.",num);
        return;
    }
    if(num<(*raiz)->valor){ /* Se o numero for menor que a raiz, ele eh inserido na esquerda */
        inserir(&(*raiz)->esq,num);
        return;
    }
    if(num>(*raiz)->valor){ /* Se o numero for maior que a raiz, ele eh inserido na direita*/
        inserir(&(*raiz)->dir,num);
        return;
    }
    printf("\nO elemento %d ja existe na arvore\n",num); /* Se nao entrar em nenhum dos ifs eh pq ja existe */
}

int main(){
    No *raiz;
    return 0;
}
