#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct livro Livro;
struct livro{
    char nome[30];
    int ano;
    Livro *prox;
};

Livro *aloca(){
    return malloc(sizeof(Livro));
}

void adiciona(Livro *cabeca){
    Livro *aux, *novo;
    aux=cabeca;
    while(aux->prox!=NULL){
        aux = aux->prox;
    }
    novo = aloca();
    fflush(stdin);
    system("cls");
    printf("Nome do livro: ");
    fgets(novo->nome,30,stdin);
    fflush(stdin);
    printf("Ano do livro: ");
    scanf("%d", &novo->ano);
    novo->prox=NULL;
    aux->prox = novo;
    system("cls");
}

void listar(Livro *celula){
    if(celula!=NULL){
        printf("Nome: %sAno: %d\n",celula->nome,celula->ano);
        printf("------------------------------------\n");
        listar(celula->prox);
    }
    else{printf("Nao ha livros no acervo\n\n");}
}

void procura(Livro *celula){
    char busca[30];
    Livro *p;
    p=celula;
    system("cls");
    printf("Nome do livro a ser buscado: ");
    fgets(busca,29,stdin);
    while(p!=NULL && strcmp(busca,p->nome)!=0){
        p=p->prox;
    }
    if(p!=NULL){
        printf("Livro encontrado \n Nome: %sAno: %d\n",p->nome,p->ano);
    }
    else{printf("Livro nao encontrado\n");}
}

int main(){
    int op=0;
    Livro acervo;
    acervo.prox=NULL;
    do{
        printf("1 - Adicionar Livro\n");
        printf("2 - Listar livros\n");
        printf("3 - Procurar um livro\n");
        printf("0 - Sair\n");
        printf("SUA OPCAO: ");
        scanf("%d",&op);
        switch(op){
            case 1: adiciona(&acervo);
                break;
            case 2:
                system("cls");
                listar(acervo.prox);
                break;
            case 3:
                procura(acervo.prox);
                break;
            default:
                system("cls");
                printf("Opcao invalida\n\n");
        }
    }while(op!=0);
    return 0;
}
