#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MAX 4

int main(){
    int *pt,linhasA,colunasA,linhasB,colunasB, controle=0;
    do{
        /*----------Preenchendo A-----------*/
        printf("Quantidade de linhas de A: ");
        scanf("%d",&linhasA);
        printf("Colunas de A: ");
        scanf("%d",&colunasA);
        if(colunasA==MAX){
            int a[linhasA][colunasA];
            preencher(linhasA, colunasA, a);
            controle=1;
        }
        else{
            printf("\nColunas de A nao podem ser diferentes de 4\n");
            system("pause");
        }
        system("cls");
    }while(controle==0);

    do{
        controle=0;
        /*----------Preenchendo B-----------*/
        printf("Quantidade de linhas de B: ");
        scanf("%d",&linhasB);
        printf("Colunas de B: ");
        scanf("%d",&colunasB);
        if(linhasB==MAX){
            int b[linhasB][colunasB];
            preencher(linhasB, colunasB, b);
            controle=1;
        }
        else{
            printf("\nLinhas de B nao podem ser diferentes de 4\n");
            system("pause");
        }
        system("cls");
    }while(controle==0);
}


void preencher(int linhas, int colunas, int *pt){
    int i, j;
    srand(time(NULL));
    for(i=0;i<linhas;i++){
        for(j=0;j<colunas;j++){
            x[i][j]=rand()%7;
        }
    }
}

void mostrar(int linhas, int colunas, int x[][MAX]){
    int i, j;
    for(i=0;i<linhas;i++){
        for(j=0;j<colunas;j++){
            printf("\n%d",x[i][j]);
        }
    }
}

void multiplicar(int linhas,int a[][MAX], int b[][MAX]){
    int colunas=linhas;
    int i,j,c[linhas][colunas];
    for(i=0;i<linhas+1;i++){
        for(j=0;j<colunas+1;j++){
            c[i][j]=(a[i][j]*a[i+1][j])+(b[i][j]*b[i+1][j]);
        }
    }
    mostrar(linhas, colunas, c);
}
