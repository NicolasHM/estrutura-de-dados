#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main(){
    int n;
    printf("\n\nDigite um inteiro\n");
    scanf("%d",&n);
    if(ehprimo(n)){
        printf("\n\nEh primo\n");
        vizinhos(n);
    }
    else{
        printf("\n\nNao eh primo\n");
        main();
    }
}

/* Identifica se for primo e retorna 1 */
int ehprimo(int n){
    int d;
    for(d=n;d>0;d--){
        if(n%d==0 && n!=d || n==1){
            return 0;
            break;
        }
        else if(d==2){
            return 1;
            break;
        }
    }
}

/* Determina os vizinhos de n */
int vizinhos(int n){
    int c = n;
    int b = n;
    while(c>0){
        c--;
        if(ehprimo(c)){
            printf("\n\nAnterior: %d",c);
            break;
        }
    }
    while(b>0){
        b++;
        if(ehprimo(b)){
            printf("\n\nPosterior: %d\n",b);
            break;
        }
    }
}
