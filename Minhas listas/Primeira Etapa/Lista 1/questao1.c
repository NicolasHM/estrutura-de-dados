#include <stdio.h>
#include <stdlib.h>
int main(){
    /* Declarando variaveis */
    int i, aux, pares, impares, qt, total, parar;
    int nums[7];
    float media;

    /* Iniciando variaveis */
    pares=0;
    impares=0;
    qt=0;
    total=0;
    media=0.0;

    /* preenchendo o vetor */
    printf("Digite sete numeros\n");
    for(i=0;i<7;i++){
        scanf("%d",&nums[i]);
        if(nums[i]<256 && nums[i]%2!=0 && nums[i]>50){
            total+=nums[i];
            qt++;
            media = total/qt;
        }
    }


    /* Ordenando o vetor */
    while(parar==0){
        parar = 1;
        for(i=0;i<6;i++){
            if(nums[i]<nums[i+1]){
                parar=0;
                /* troca de lugares */
                aux = nums[i];
                nums[i]=nums[i+1];
                nums[i+1]=aux;
            }
        }
    }

    /* Exibindo resultados */
    printf("\n\n\n");
    for(i=0;i<7;i++){
        if(nums[i]<256){
            printf("%d ",nums[i]);
            if(nums[i]%2==0){
                pares++;
            }
            else{
                impares++;
            }
        }
    }
    printf("\n\nPares:%d Impares:%d Media:%.2f",pares,impares,media);
}
