from math import sqrt
lista = [2, 3, 5, 7, 8, 9, 15, 19, 21, 82]
lista2 = [16,18,22,22,25,25,27,34,38,39,40]

def somatorio(lista):
    aux = 0
    for i in lista:
        aux+=i
    return aux

def media(lista):
    soma = somatorio(lista)
    return round(soma/len(lista),2)

def variancia(lista):
    desvios_sqr = []
    med = media(lista)
    for i in lista:
        aux = i - med
        aux = aux*aux
        desvios_sqr.append(aux)
    return round(media(desvios_sqr),2)

def desvio_padrao(lista):
    return round(sqrt(variancia(lista)),2)

def mediana(lista):
    med = (len(lista)//2)
    if len(lista)%2 != 0:
        return lista[med]
    else:
        return (lista[med]+lista[med-1])/2

def coef_var(lista):
    return round(desvio_padrao(lista)/media(lista),2)

def moda(lista):
    modas=[]
    for i in lista:
        aux = lista.count(i)
        if(aux>1 and i!=ant): modas.append(i)
        ant = i
    if modas == []: return "amodal"
    else: return modas

def printa(lista):
    print("dados: ",lista)
    print("variancia: ",variancia(lista))
    print("media: ",media(lista))
    print("mediana: ",mediana(lista))
    print("desvio: ",desvio_padrao(lista))
    print("moda: ", moda(lista))
    print("coef: ", coef_var(lista))

printa(lista)
print("-------------------------------------------")
printa(lista2)
