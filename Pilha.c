#include <stdio.h>
#include <stdlib.h>

int tam = 0;

typedef struct node Node;
struct node{
    int valor;
    Node *prox;
};

int vazia(Node *pilha){
    if(pilha->prox == NULL){
        return 1;
    }
    else{return 0;}
}

Node* aloca(){
    return (Node*)malloc(sizeof(Node));
}

void libera(Node *pilha){
    if(vazia(pilha)){
        Node *atual, *atualProx;
        atual = pilha;
        while(atual != NULL){
            atualProx = atual->prox;
            free(atual);
            atual = atual->prox;
        }
    }
}

void exibe(Node *pilha){
    if(vazia(pilha)){
        printf("Pilha vazia!");
        return;
    }
    Node *aux = pilha->prox;
    while(aux!=NULL){
        printf("%d, ",aux->valor);
        aux = aux->prox;
    }
}

void push(Node *pilha, int val){
    Node *novo, *aux;

    novo = aloca();
    novo->valor = val;
    novo->prox = NULL;

    aux = pilha;
    while(aux->prox!=NULL){
        aux = aux->prox;
    }
    aux->prox = novo;
    tam++;
}

void pop(Node *pilha){
    if(vazia(pilha)){
        printf("Pilha vazia!\n");
        return;
    }
    else{
        Node *ultimo, *penultimo;
        penultimo = pilha;
        ultimo = penultimo->prox;
        while(ultimo->prox != NULL){
            penultimo = penultimo->prox;
            ultimo = ultimo->prox;
        }
        penultimo->prox = NULL;
        free(ultimo);
    }
    tam--;
}

void menu(Node *pilha){
    int op = 0, val;
    do{
        printf("----------- PILHA ------------\n");
        printf("1 - Push\n");
        printf("2 - Mostrar pilha\n");
        printf("3 - Pop\n");
        printf("0 - Sair\n\n");
        printf("TAMANHO DA PILHA: %d\n",tam);
        printf("Digite a opcao: ");
        scanf("%d",&op);
        switch(op){
            case 1:
                system("cls");
                printf("Digite o valor: "); scanf("%d",&val);
                push(pilha, val);
                system("cls");
                printf("Item adicionado.\n");
                break;
            case 2:
                system("cls");
                exibe(pilha);
                printf("\n");
                system("pause");
                system("cls");
                break;
            case 3:
                system("cls");
                pop(pilha);
                break;
            default:
                system("cls");
                printf("Opcao invalida\n\n");
                break;
        }
    }while(op != 0);
    system("cls");
    printf("Ate mais!\n");
}

int main(){
    Node *pilha = aloca();
    pilha->prox = NULL;
    menu(pilha);
    free(pilha);
    return 0;
}
