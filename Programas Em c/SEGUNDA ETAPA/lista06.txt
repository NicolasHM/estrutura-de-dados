#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

typedef struct NO* ArvBin;
struct NO{
    int info;
    struct NO *esq;
    struct NO *dir;
};

//Funcao para criar raiz
ArvBin *cria_ArvBin(){
    ArvBin *raiz = (ArvBin*)malloc(sizeof(ArvBin));
    if (raiz != NULL){
        *raiz = NULL;
    }
    return raiz;
}

//Funcao para liberar no
void libera_NO(struct NO* no){
    if (no == NULL){
        return;
    }
    libera_NO(no->esq);
    libera_NO(no->dir);
    free(no);
    no = NULL;
}

//Funcao para liberar arvore
void libera_ArvBin(ArvBin *raiz){
    if (raiz == NULL){
        return;
    }
    libera_NO(*raiz);
    free(raiz);
    printf("Arvore esvaziada com sucesso!");
}

//Funcao que verifica se arvore esta vazia
int vazia(ArvBin *raiz){
    if (raiz == NULL){
        printf("Arvore vazia!\n");
        return 1;
    }
    if (*raiz == NULL){
        printf("Arvore vazia!\n");
        return 1;
    }
    printf("Arvore n�o vazia!\n");
    return 0;
}

//Funcao que percorre arvore em pr� ordem
void preOrdem(ArvBin *raiz){
    if (raiz == NULL){
        return;
    }
    if (*raiz != NULL){
        printf("%d\n", (*raiz)->info);
        preOrdem(&((*raiz)->esq));
        preOrdem(&((*raiz)->dir));
    }
}

//Fun��o que percorre �rvore EM ORDEM
void emOrdem(ArvBin *raiz){
    if (raiz == NULL){
        return;
    }
    if(*raiz != NULL){
        emOrdem(&((*raiz)->esq));
        printf("%d\n", (*raiz)->info);
        emOrdem(&((*raiz)->dir));
    }
}

//Fun��o que percorre raiz em P�S ORDEM
void posOrdem(ArvBin *raiz){
    if (raiz == NULL){
        return;
    }
    if (*raiz != NULL){
        posOrdem(&((*raiz)->esq));
        posOrdem(&((*raiz)->dir));
        printf("%d\n", (*raiz)->info);
    }
}

//Fun��o que insere um elemento na arvore Bin�ria de busca
int insere(ArvBin *raiz, int valor){
    //Se a raiz for nula, ele usa malloc para um novo elemento
    if (raiz == NULL){
        return 0;
    }
    struct NO *novo;
    novo = (struct NO*)malloc(sizeof(struct NO));
    if (novo == NULL){
        return 0;
    }
    novo->info = valor;
    novo->dir = NULL;
    novo->esq = NULL;
    if (*raiz == NULL){
        *raiz = novo;
    }
    //Se n�o ele percorre a arvore at� encontrar o valor
    else{
        struct NO *atual = *raiz;
        struct NO *ant = NULL;
        while(atual != NULL){
            ant = atual;
            if (valor == atual->info){
                free(novo);
                return 0;
            }
            if (valor > atual->info){
                atual = atual->dir;
            }else{
                atual = atual->esq;
            }
        }
        if (valor > ant->info){
            ant->dir = novo;
        }else{
            ant->esq = novo;
        }
    }
    return 1;
}

struct NO *removendo_atual(struct NO* atual){
    struct NO *no1, *no2;
    //Se o n� s� tiver 1 filho
    if (atual->esq == NULL){
        no2 = atual->dir;
        free(atual);
        return no2;
    }
    //Procura o filho mais a direita da subarvore esquerda para excluir
    no1 = atual;
    no2 = atual->esq;
    while(no2->dir != NULL){
        no1 = no2;
        no2 = no2->dir;
    }
    //Copia o filho mais a direita da subarvore esquerda para colocar no lugar
    if (no1 != atual){
        no1->dir = no2->esq;
        no2->esq = atual->esq;
    }
    no2->dir = atual->dir;
    free(atual);
    return no2;
}

//Fun��o que remove um elemento da �rvore bin�ria
int remover(ArvBin *raiz, int valor){
    if (raiz == NULL){
        return 0;
    }
    struct NO *ant = NULL;
    struct NO *atual = *raiz;
    while (atual != NULL){
        //Se o valor for encontrado
        if (valor == atual->info){
            if (atual == *raiz){
                *raiz = removendo_atual(atual);
            }
            else{
                if (ant->dir == atual){
                    ant->dir = removendo_atual(atual);
                }else{
                    ant->esq = removendo_atual(atual);
                }
            }
            return 1;
        }
        //Continua percorrendo a �rvore at� encontrar o n�
        ant = atual;
        if (valor > atual->info){
            atual = atual->dir;
        }
        else{
            atual = atual->esq;
        }
    }
}

//FUn��o que faz a consulta de uma arvore bin�ria de pesquisa
int busca(ArvBin *raiz,int valor){
    if (raiz == NULL){
        return 0;
    }
    struct NO *atual = *raiz;
    while (atual != NULL){
        if (valor == atual->info){
            printf("Valor encontrado!\n");
            return 1;
        }
        if (valor > atual->info){
            atual = atual->dir;
        }
        else{
            atual = atual->esq;
        }
    }
    return 0;
}

void inserirNos(ArvBin *raiz){
int item;
while (1){
    system("cls");
    printf("Digite um valor a ser inserido, -1 pra encerrar\n");
    scanf("%d", &item);
    if (item == -1){
        break;
    }
    insere(raiz, item);
}
}

//FUN��O PRINCIPAL
int main(){
    setlocale(LC_ALL, "Portuguese");
    ArvBin *arvore;
    int escolha, valor,x,y,w,z, opcao;
    while(1){
        printf("\nMenu principal - Arvore Binaria de busca\n");
        printf("\n1)Criar uma arvore vazia\n");
        printf("\n2)Verificar se a arvore esta vazia\n");
        printf("\n3)Inserir um item\n");
        printf("\n4)Consultar um item na arvore\n");
        printf("\n5)Remover um item da arvore\n");
        printf("\n6)Esvaziar arvore\n");
        printf("\n7)Imprimir a �rvore\n");
        printf("\n8)Sair do programa\n");
        scanf("%d", &escolha);
        switch (escolha){
        case 1:
            system("cls");
            arvore = cria_ArvBin();
            printf("Arvore criada com sucesso!!\n");
            break;
        case 2:
            system("cls");
            x = vazia(arvore);
            break;
        case 3:
            inserirNos(arvore);
            break;
        case 4:
            system("cls");
            printf("Digite um valor: ");
            scanf("%d", &valor);
            z = busca(arvore, valor);
            break;
        case 5:
            system("cls");
            printf("Digite um valor a ser removido: ");
            scanf("%d", &valor);
            w = remover(arvore, valor);
            printf("Valor removido com sucesso!\n");
            break;
        case 6:
            system("cls");
            libera_ArvBin(arvore);
            break;
        case 7:
            system("cls");
            printf("1)Pr�-ordem\n");
            printf("2)Em ordem\n");
            printf("3)Pos-ordem\n");
            printf("4)Sair do programa\n");
            printf("Digite sua opcao: ");
            scanf("%d", &opcao);
            switch(opcao){
            case 1:
                preOrdem(arvore);
                break;
            case 2:
                emOrdem(arvore);
                break;
            case 3:
                posOrdem(arvore);
                break;
            case 4:
                return;
            }
            break;
        case 8:
            return;

        default:
            printf("Insira uma op��o v�lida");
            break;
    }
}
}
