#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct box Box;
    struct box{
        char conteudo;
        Box *proximo;
    };
    Box *inicio,*temp1,*temp2,*novo;

void imprimir(Box *inicio, char conteudo){
 temp1 = inicio;
    while (temp1 != NULL) {
        printf("%c  ", temp1->conteudo);//imprimindo o conteudo de temp1
        temp1 = temp1->proximo;// incrementando para o proximo endere�o
    }
}
int main()
{
    //PRIMEIRA QUEST�O
    inicio = (Box*)malloc(sizeof(Box));
    inicio->conteudo='A';
    inicio->proximo = NULL; //Proxima estrutura recebe NULL
    printf("Colocando o A no final da lista:\n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    //SEGUNDA QUEST�O
    novo=(Box*)malloc(sizeof(Box));
    novo->conteudo='B';
    novo->proximo=inicio;//NOVO PROXIMO RECEBE ENDERE�O DO INICIO
    inicio=novo;//INICIO RECEBE ENDERE�O DO NOVO / TROCA DE ENDERE�OS
    printf("Colocando o B no inicio da lista:\n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    //TERCEIRA QUEST�O
    temp2 = inicio;
    while (temp2->proximo != NULL){
        temp2 = temp2->proximo;
    } //ACHANDO O ULTIMO DA LISTA;
    novo=(Box*)malloc(sizeof(Box));
    novo->conteudo = 'C';
    temp2->proximo = novo;
    novo->proximo = NULL;//ULTIMO APONTANDO SEMPRE PRA NULL
    printf("Colocando o C no final da lista:\n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    //QUARTA QUEST�O
     temp2 = inicio;
    while (temp2->proximo != NULL){
        temp2 = temp2->proximo;
    } //ACHANDO O ULTIMO DA LISTA;
    novo=(Box*)malloc(sizeof(Box));
    novo->conteudo = 'D';
    temp2->proximo = novo;
    novo->proximo = NULL; //ULTIMO APONTANDO SEMPRE PRA NULL
    printf("Colocando o D no final da lista:\n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    //QUINTA QUEST�O
    temp2 = inicio;
    while (temp2->conteudo != 'A'){
        temp2 = temp2->proximo;
    }
    temp1 = (Box*)malloc(sizeof(Box));//ALOCANDO AUXILIAR
    temp1 = temp2->proximo;//APONTA PRA C
    novo = (Box*)malloc(sizeof(Box));;
    novo->conteudo = 'E';
    temp2->proximo = novo;//RECEBENDO O ENDERE�O QUE APONTA PRA E NA FRENTE DO ENDERE�O DE A
    novo->proximo = temp1;
    printf("Colocando o E entre A e C \n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    //SEXTA QUEST�O
       temp2 = inicio;
    while (temp2->proximo != NULL){
        temp2 = temp2->proximo;
    } //ACHANDO O ULTIMO DA LISTA;
    novo=(Box*)malloc(sizeof(Box));
    novo->conteudo = 'F';
    temp2->proximo = novo;
    novo->proximo = NULL;//ULTIMO APONTANDO SEMPRE PRA NULL
    printf("Colocando o F no final da lista:\n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    //SETIMA QUEST�O
    novo=(Box*)malloc(sizeof(Box));
    novo->conteudo='G';
    novo->proximo=inicio;//NOVO PROXIMO RECEBE ENDERE�O DO INICIO
    inicio=novo;//INICIO RECEBE ENDERE�O DO NOVO / TROCA DE ENDERE�OS
    printf("Colocando o G no inicio da lista:\n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    //OITAVA QUEST�O
    temp2 = inicio;
    while(temp2->proximo->proximo != NULL){
        temp2 = temp2->proximo;
    } //SAIO COM ENDERE�O DO PENULTIMO

    temp1 = (Box*)malloc(sizeof(Box)); //ALOCANDO AUXILIAR
    temp1->proximo = temp2->proximo;
    free(temp1);
    temp2->proximo = NULL;
    printf("Excluindo o ultimo da lista:\n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    //NONA QUEST�O
    temp1 = (Box*)malloc(sizeof(Box));//ALOCANDO AUXILIAR
    temp1 = inicio->proximo;//APONTANDO PARA O SEGUNDO ELEMENTO
    free(inicio);//LIBERANDO A MEMORIA DO PRIMEIRO;
    inicio=temp1;//ININCIO RECEBE ENDERE�O DO TEMP1 QUE � O PRIMEIRO AGORA;
    printf("Excluindo o primeiro da lista:\n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    //D�CIMA QUEST�O
    temp2 = inicio;
    while (temp2->conteudo != 'A'){
        temp2 = temp2->proximo;
    }
    temp1 = (Box*)malloc(sizeof(Box));//ALOCANDO AUXILIAR
    temp1 = temp2->proximo;
    novo = (Box*)malloc(sizeof(Box));;
    novo->conteudo = 'H';
    temp2->proximo = novo;
    novo->proximo = temp1;
    printf("Colocando o H entre A e E: \n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    //D�CIMA PRIMEIRA QUEST�O
    temp2 = inicio;
    while(temp2->conteudo!='A'){
        temp2 = temp2->proximo;
    } //DESCOBRINDO O ENDERE�O DE A

    inicio->proximo = temp2->proximo; //
    free(temp2);
    printf("EXCLUINDO  A: \n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    //D�CIMA SEGUNDA QUEST�O
     temp2 = inicio;
    while (temp2->conteudo != 'E'){
        temp2 = temp2->proximo;
    }
    temp1 = (Box*)malloc(sizeof(Box));
    temp1 = temp2->proximo;
    novo = (Box*)malloc(sizeof(Box));;
    novo->conteudo = 'I';
    temp2->proximo = novo;
    novo->proximo = temp1;
    printf("Colocando o I entre E e D: \n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    //D�CIMA TERCEIRA QUEST�O
    temp2 = inicio;
    while (temp2->proximo != NULL){
        temp2 = temp2->proximo;
    } //ACHANDO O ULTIMO DA LISTA;
    novo=(Box*)malloc(sizeof(Box));
    novo->conteudo = 'J';
    temp2->proximo = novo;
    novo->proximo = NULL;
    printf("Colocando o J no final da lista:\n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    //D�CIMA QUARTA QUEST�O
    temp2 = inicio;
    while (temp2->conteudo != 'B'){
        temp2 = temp2->proximo;
    }
    temp1 = (Box*)malloc(sizeof(Box));
    temp1 = temp2->proximo;
    novo = (Box*)malloc(sizeof(Box));;
    novo->conteudo = 'K';
    temp2->proximo = novo;
    novo->proximo = temp1;
    printf("Colocando o K entre B e H: \n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    //D�CIMA QUINTA QUEST�O
        //EXCLUINDO O D
    temp2=inicio;
    while(temp2->conteudo!='C'){
        temp2 = temp2->proximo;
    }
    temp1 = (Box*)malloc(sizeof(Box));
    temp1 = temp2->proximo;
    temp2->proximo = temp1->proximo;
    free(temp1);
        //EXCLUINDO O K
    temp2=inicio;
    while(temp2->conteudo!='B'){
        temp2 = temp2->proximo;
    }
    temp1 = (Box*)malloc(sizeof(Box));
    temp1 = temp2->proximo;
    temp2->proximo = temp1->proximo;
    free(temp1);
        //EXCLUINDO O I
    temp2=inicio;
    while(temp2->conteudo!='E'){
        temp2 = temp2->proximo;
    }
    temp1 = (Box*)malloc(sizeof(Box));
    temp1 = temp2->proximo;
    temp2->proximo = temp1->proximo;
    free(temp1);
    printf("Excluindo o D, K , I: \n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    //D�CIMA SEXTA QUEST�O
        //EXCLUINDO O PRIMEIRO QUE � O B
    temp1 = (Box*)malloc(sizeof(Box));//ALOCANDO AUXILIAR
    temp1 = inicio->proximo;//APONTANDO PARA O SEGUNDO ELEMENTO
    free(inicio);//LIBERANDO A MEMORIA DO PRIMEIRO;
    inicio=temp1;//INICIO RECEBE ENDERE�O DO TEMP1 QUE � O PRIMEIRO AGORA;
    printf("Excluindo o B da lista:\n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    //D�CIMA S�TIMA QUEST�O
    temp2 = inicio;
    while (temp2->proximo != NULL){
        temp2 = temp2->proximo;
    } //ACHANDO O ULTIMO DA LISTA;
    novo=(Box*)malloc(sizeof(Box));
    novo->conteudo = 'L';
    temp2->proximo = novo;
    novo->proximo = NULL;
    printf("Colocando o L no final da lista:\n");
    imprimir(inicio,inicio->conteudo);
    printf("\n-------------------------------------\n");

    return 0;
}
