#include <stdlib.h>
#include <stdio.h>

#define tam 11

typedef struct reg Reg;
struct reg{
    Reg *prox;
    char letra;
    int cpf;
};
typedef Reg* Hash[tam];

//hashing modular
int func_hash(int num){
    return (num & 0x7FFFFFFF) % tam;
}

void inicia_hash(Hash tabela){
    for(int i=0 ; i<tam ; i++){
        tabela[i] = NULL;
    }
}

void insere(Hash tabela, char let){
    int chave = func_hash( (int)let );
    Reg* aux = tabela[chave];
    while(aux!=NULL){
        if(aux->letra == let){
            break;
        }
        aux = aux->prox;
    }
    if(aux == NULL){
        aux = (Reg*)malloc(sizeof(Reg));
        aux->letra = let;
        aux->cpf = 100+rand()%899;
        aux->prox = tabela[chave];
        tabela[chave] = aux;
    }
}

int procurar(Hash tabela, char let){
    int chave = func_hash( (int)let );
    Reg* aux = tabela[chave];
    if(aux == NULL ){return;}
    while(aux != NULL){
        if(aux->letra == let){
            return aux->cpf;
        }
        aux=aux->prox;
    }
    return;
}

void menu(Hash tab){
    int cpf,op=2;
    char letra;
    while(op != 0){
        printf("------TABELA HASH------\n");
        printf("1 - Inserir na tabela\n");
        printf("2 - Procurar na tabela\n");
        printf("3 - Excluir da tabela\n");
        printf("0 - SAIR\n");
        printf("Sua opcao: ");
        scanf("%d",&op);
        switch(op){
            case 1:
                system("cls");
                fflush(stdin);
                printf("Qual eh a letra? ");
                scanf("%c", &letra);
                insere(tab, letra);
                break;
            case 2:
                system("cls");
                fflush(stdin);
                printf("Qual eh a letra? ");
                scanf("%c", &letra);
                cpf = procurar(tab,letra);
                system("cls");
                if(cpf!=0){
                    printf("Cpf de %c: %d\n", letra, cpf);
                }
                else{
                    printf("Nao esta na tabela\n");
                }
                break;
            default:
                system("cls");
                printf("Opcao invalida\n");
        }
    }
}

int main(){
    Hash tab[tam];
    inicia_hash(tab);
    menu(tab);
    return 0;
}
