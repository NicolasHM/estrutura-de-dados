#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#define range 30

void fill_vetor(int *v, int tam)
{
    srand(time(NULL));
    int *pos_1=v;
    printf("tam : %d\n",tam);
    while(v-pos_1!=tam)
    {
        *v = rand()%range;
        v++;
    }
}

void printv(int vetor[], int tam)
{
    int i;
    for(i=0;i<tam;i++)
    {
        printf(" Ind: %d --- Valor: %d\n",i,vetor[i]);
    }
}

int clear_repetidos(int *vetor, int tam)
{
    int i,j,aux,cont=0, warn;
    bool controle1=false, controle2=true,controle3;
    for(i=0;i<tam;i++)
    {
        printf("Executando o For com i=%d...\n\n",i);
        controle2=true;
        controle3=false;
        while(controle2==true)
        {
            printf("Executando o while, i=%d...\n\n",i);
            j=i+1;
            do
            {
                printf("\n\nvetor[%d]: %d == vetor[%d]: %d...\n",i,vetor[i],j,vetor[j]);
                if(vetor[i]==vetor[j])
                {
                    printf("Sim, trocando valores...\n\n");
                    aux=vetor[i];
                    printf("Aux recebe vetor[i=%d] %d\n",i,vetor[i]);
                    if(j+1<tam)
                    {
                        printf("Vetor[j=%d](%d) recebe valor do proximo(%d): %d\n",j,vetor[j],j+1,vetor[j+1]);
                        vetor[j]=vetor[j+1];
                        printf("Vetor[j=%d] recebe valor de aux(%d): %d\n\n",j+1,aux,vetor[j+1]);
                        vetor[j+1]=aux;
                        controle1=true;
                        controle2=true;
                        warn = 0;
                    }
                    else
                    {
                        printf("Opa! Limite do vetor ultrapassado! Nao efetuando troca...\n");
                        if(warn==1)
                        {
                            controle3=true;
                            tam--;
                            cont++;
                            break;
                        }
                        controle1=true;
                        controle2=true;
                        warn=1;
                    }
                }
                else
                {
                    printf("Nao, passando para o proximo...\n\n");
                    controle2=false;
                }
                system("pause");
                j++;
            }while(j<tam);
            if(controle1)
            {
                printf("\n\nAumentando o cont e diminuindo o tam...\n\n");
                cont++;
                tam--;
                controle1=false;
            }
            if(controle3)
            {
                break;
            }
            system("pause");
            system("cls");
        }
        system("pause");
        system("cls");
    }
    if(cont!=0)
    {
        printf("\n Existiam %d numeros repitidos!",cont);
    }else printf("\n Nao exisitam numeros repitidos!");
    printf("\n\n");
    printv(vetor,tam+cont);
    printf("\n\n");
    printv(vetor,tam);
    return tam;
}

void fill_novo_vetor(int tam1, int *novo,int *vetor)
{
    int i;
    for(i=0;i<tam1;i++)
    {
        novo[i]=vetor[i];
    }
}

void ordena(int tam, int *v)
{
    int i,j,aux;
    for(i=0;i<tam;i++)
    {
        for(j=0;j<tam;j++)
        {
            if(v[i]<v[j])
            {
                aux = v[i];
                v[i]=v[j];
                v[j]=aux;
            }
        }
    }
}

int Busca_bin(int tam, int v[], int valor)
{
    int ini,meio,fim;
    ini=0;
    meio=tam/2;
    fim=tam-1;
    while(fim>=ini)
    {
        if(valor==v[meio])
        {
            return meio;
        }
        else if(valor<v[meio])
        {
            fim=meio-1;
            meio=(ini+fim)/2;
        }
        else
        {
            ini=meio+1;
            meio=(ini+fim)/2;
        }
    }
    return -1;
}

int main()
{
    int tam;
    printf("Digite o tamanho do vetor: ");
    scanf("%d",&tam);
    int vetor[tam];
    fill_vetor(vetor,tam);
    printf("Tamanho antes da retirada dos repetidos: %d\n",tam);
    int novo_tam;
    novo_tam=clear_repetidos(vetor,tam);
    printf("Tamanho depois da retirada dos repetidos: %d\n",novo_tam);
    int novo_vetor[novo_tam];
    fill_novo_vetor(novo_tam,novo_vetor,vetor);
    printv(novo_vetor,novo_tam);
    ordena(novo_tam,novo_vetor);
    printf("Vetor novo ordenado\n");
    printv(novo_vetor,novo_tam);
    int valor;
    printf("Digite um valor: ");
    scanf("%d",&valor);
    int res;
    res=Busca_bin(novo_tam,novo_vetor,valor);
    if(res!=-1)
    {
        printf("O valor %d esta no vetor no indice %d",valor,res);
    }
    else
    {
        printf("O valor %d nao pertence ao vetor!",valor);
    }
    return 0;
}
